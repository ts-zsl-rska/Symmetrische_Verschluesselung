import javax.swing.JOptionPane;

/**
  *
  * Beschreibung
  * abstrakte Klasse für die Implementierung einer beliebigen Verschlüsselung, die
  * in das Chat-Programm eingebaut werden kann
  * @version 1.0 vom 22.6.2017
  * @author Thomas Schaller
  */

public abstract class Verschluesselung {

  // Anfang Attribute
 
  // Ende Attribute
  
  public Verschluesselung() {
  
  }
  
  // Anfang Methoden
  
  /* getNummer bestimmt die Nummer des Buchstaben im Alphabet A=0, B=1, ... Z=25 , a=0, b=0,...z=25
  */
  
  protected int getNummer(char c) {
    int ascii = (int) c;   // in ASCII Code umwandeln
    int nr = 0;
    if (ascii < 96) {  // Gro�buchstaben
      nr = ascii-65;
    } else {           // Kleinbuchstaben
      nr = ascii-97;
    }// end of if
    return nr;
  }
  
  /* getChar liefert den Buchstaben zur Position im Alphabet 0=>A, 1=>B usw.
  */
  protected char getChar(int nr) {
    int ascii = nr +65;
    char c = (char) ascii;
    return c;
  }
  
  /* Wandelt einen Schl�ssel-String in ein Integerzahl um (z.B. aus "123"=>123). Im Fehlerfall wird eine Meldung ausgegeben und 0 zur�ckgegeben. 
  */
  protected int convertStringToInt(String s) {
    int i = 0;
    try {
      i = Integer.parseInt(s);
    } catch(Exception e) {
      JOptionPane.showMessageDialog(null,"Der eingegebene Schl�ssel muss eine Zahl sein.","Fehler",JOptionPane.ERROR_MESSAGE);

    } // end of try
    return i;
  }


  /* Methoden zur Ver/Entschl�sselung. Sie werden vom Chatclient benutzt */
  public abstract String encode(String klartext, String schluessel);
  public abstract String decode(String kryptotext, String schluessel);
  
  /* Liefert den Namen der Verschl�sselung, um sie im Chatclient anzeigen zu k�nnen */
  public abstract String getName();
  
  
  // Ende Methoden
  
}
