

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Die Test-Klasse VigenereVerschluesselungTest.
 *
 * @author  (Ihr Name)
 * @version (eine Versionsnummer oder ein Datum)
 */
public class VigenereVerschluesselungTest
{
    /**
     * Konstruktor fuer die Test-Klasse VigenereVerschluesselungTest
     */
    public VigenereVerschluesselungTest()
    {
    }

    /**
     *  Setzt das Testgerüst fuer den Test.
     *
     * Wird vor jeder Testfall-Methode aufgerufen.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Gibt das Testgerüst wieder frei.
     *
     * Wird nach jeder Testfall-Methode aufgerufen.
     */
    @After
    public void tearDown()
    {
    }

    @Test
    public void verschluesseln()
    {
        VigenereVerschluesselung vigenere1 = new VigenereVerschluesselung();
        assertEquals("HBLMO", vigenere1.encode("HALLO", "AB"));
        assertEquals("PNQZWUJFZFHVIYQSZ", vigenere1.encode("HALLO HERR SCHALLER", "Info"));
        assertEquals("CDEZAB", vigenere1.encode("abcxyz", "c"));
    }
    
    @Test
    public void entschluesseln()
    {
        VigenereVerschluesselung vigenere1 = new VigenereVerschluesselung();
        assertEquals("HALLO", vigenere1.decode("HBLMO", "AB"));
        assertEquals("HALLOHERRSCHALLER", vigenere1.decode("PNQZWUJFZFHVIYQSZ", "Info"));
        assertEquals("ABCXYZ", vigenere1.decode("CDEZAB", "c"));
    }
}

