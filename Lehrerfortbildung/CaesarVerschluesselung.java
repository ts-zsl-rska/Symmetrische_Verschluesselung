/**
 *
 * Beschreibung
 * Caesar Verschuesselung zur Verwendung im Chatclient
 *
 * @version 1.0 vom 22.6.2017
 * @author Thomas Schaller
 */

public class CaesarVerschluesselung extends Verschluesselung{

    public CaesarVerschluesselung() {
        super();
    }

    // Anfang Methoden
    public String getName() {
        return "Caesar-Verschlüsselung";
    }

    /**
     * Verschlüsselt den übergebenen Klartext mit dem Cäsar-Verschlüsselungsalgorithmus.
     *
     * @param klartext  Der zu verschlüsselnde Klartext.
     * @param schluessel Der Schlüssel, der für die Verschlüsselung verwendet wird.
     * @return Der verschlüsselte Kryptotext.
     */
    public String encode(String klartext, String schluessel)
    {
        /*#
        String-Befehle:
        s.length()    liefert die Laenge des Strings
        s.charAt(nr)  liefert den Buchstaben (char) an Position nr (man beginnt mit 0 zu zaehlen)
        s = s + "en"; erweitert den String um den String en
        s = s + 'n';  erweitert den String um den String en
        s.toUpperCase() in Grossbuchstaben umwandeln
        s.replaceAll()  Buchstaben ersetzen
        s.toCharArray() in Array von Buchstaben umwandeln
         */

        //# Nur mit Grossbuchstaben arbeiten
        klartext = klartext.toUpperCase();
        klartext = klartext.replaceAll("[^A-Z]","");   // Alle anderen Zeichen l�schen: [] definiert einen Bereich, ^ bedeutet "alle au�er" => es werden also alle Zeichen au�er A-Z durch den leeren String ersetzt, also gel�scht
        schluessel = schluessel.toUpperCase();

        //# Wandle die Strings klartext und schluessel in ein Char-Array um
      
        //# Erzeuge ein Char-Array fuer den Kryptotext mit der gleichen Laenge wie der Klartext
       

        //# Ersten Buchstaben des Schluessels in Zahl umrechnen:
        

        //# Wiederhole fuer jeden Buchstaben des Klartextes
        
            //# Wandle i. Buchstabe in Zahl um und speichere sie in iKlar
            
            //# Verschluessele die Klarzahl mit der Schluesselzahl und speichere in iKrypto
           

            //#  Wandle iKrypto in einen Buchstabe um
            //#  Addiere dazu 'A' zu der Zahl und wandle sie explizit mit 
            //#  (char) (iKrypto+'A') in einen Buchstaben um.
            //#  Speichere den Buchstaben an der richtigen Stelle im Array aKryptotext
            
        //# Erzeuge einen neuen String aus dem Array aKryptotext
        //# verwende dazu new String(aKryptotext) und gib diesen zurueck
        
         return "";
    }

    
    /**
     * Entschlüsselt den übergebenen Kryptotext mit dem Cäsar-Verschlüsselungsalgorithmus.
     *
     * @param kryptotext Der zu entschlüsselnde Kryptotext.
     * @param schluessel Der Schlüssel, der für die Entschlüsselung verwendet wird.
     * @return Der entschlüsselte Klartext.
     */
    public String decode(String kryptotext, String schluessel)
    {
        schluessel = schluessel.toUpperCase();

        return "";
    }

    // Ende Methoden
}
