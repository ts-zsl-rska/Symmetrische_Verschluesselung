/**
 *
 * Beschreibung
 * Vigenère Verschlüsselung zur Verwendung im Chatclient
 *
 * @version 1.0 vom 22.6.2017
 * @author Thomas Schaller
 */

public class VigenereVerschluesselung extends Verschluesselung{
    public VigenereVerschluesselung() {
        super();
    }

    // Anfang Methoden
    public String getName() {
        return "Vigenère-Verschlüsselung";
    }

    public String encode(String klartext, String schluessel)
    {
        /*#
        String-Befehle:
        s.length()    liefert die Laenge des Strings
        s.charAt(nr)  liefert den Buchstaben (char) an Position nr (man beginnt mit 0 zu zaehlen)
        s = s + "en"; erweitert den String um den String en
        s = s + 'n';  erweitert den String um den String en
        s.toUpperCase() in Grossbuchstaben umwandeln
        s.replaceAll()  Buchstaben ersetzen
        s.toCharArray() in Array von Buchstaben umwandeln
         */

        //# Nur mit Grossbuchstaben arbeiten
        klartext = klartext.toUpperCase();
        klartext = klartext.replaceAll("[^A-Z]","");   // Alle anderen Zeichen löschen: [] definiert einen Bereich, ^ bedeutet "alle außer" => es werden also alle Zeichen außer A-Z durch den leeren String ersetzt, also gelöscht
        schluessel = schluessel.toUpperCase();

       return "";
    }

    public String decode(String kryptotext, String schluessel)
    {
        schluessel = schluessel.toUpperCase();

        return "";

    }
}
