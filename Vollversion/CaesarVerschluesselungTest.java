

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Die Test-Klasse CaesarVerschluesselungTest.
 *
 * @author  (Ihr Name)
 * @version (eine Versionsnummer oder ein Datum)
 */
public class CaesarVerschluesselungTest
{
    private CaesarVerschluesselung caesar;

    /**
     * Konstruktor fuer die Test-Klasse CaesarVerschluesselungTest
     */
    public CaesarVerschluesselungTest()
    {
    }

    /**
     *  Setzt das Testgerüst fuer den Test.
     *
     * Wird vor jeder Testfall-Methode aufgerufen.
     */
    @Before
    public void setUp()
    {
        caesar = new CaesarVerschluesselung();
    }

    /**
     * Gibt das Testgerüst wieder frei.
     *
     * Wird nach jeder Testfall-Methode aufgerufen.
     */
    @After
    public void tearDown()
    {
    }

    @Test
    public void verschluesseln()
    {
        assertEquals("KDOOR", caesar.encode("HALLO", "D"));
        assertEquals("KDOOR", caesar.encode("HALLO", "DOSE"));
        assertEquals("KDOOR", caesar.encode("Hallo", "D"));
        assertEquals("KDOOR", caesar.encode("HALLO", "dose"));
        assertEquals("KDOORKHUUVFKDOOHU", caesar.encode("HALLO HERR SCHALLER", "D"));
        assertEquals("WXYZAB", caesar.encode("UVWXYZ", "C"));
    }

    @Test
    public void entschluesseln()
    {
        assertEquals("HALLO", caesar.decode("KDOOR", "D"));
        assertEquals("HALLO", caesar.decode("KDOOR", "d"));
        assertEquals("HALLOHERRSCHALLER", caesar.decode("KDOORKHUUVFKDOOHU", "D"));
        assertEquals("UVWXYZ", caesar.decode("WXYZAB", "C"));
    }
}


