/**
  *
  * Beschreibung
  * ZickZack (Jägerzaun) Verschlüsselung zur Verwendung im Chatclient
  *
  * @version 1.0 vom 22.6.2017
  * @author Thomas Schaller
  */

public class ZickZackVerschluesselung extends Verschluesselung{
 
    /**
     * Standardkonstruktor.
     * Ruft den Konstruktor der Elternklasse (Verschluesselung) auf.
     */
    public ZickZackVerschluesselung() {
        super();
    }

    /**
     * Gibt den Namen der Verschlüsselungsmethode zurück.
     *
     * @return Der Name der ZickZack-Verschlüsselung.
     */
    public String getName() {
        return "ZickZack-Verschlüsselung";
    }

    /**
     * Verschlüsselt den übergebenen Klartext mit dem ZickZack-Verschlüsselungsalgorithmus.
     *
     * @param klartext  Der zu verschlüsselnde Klartext.
     * @param schluessel Der Schlüssel, der für die Verschlüsselung verwendet wird.
     * @return Der verschlüsselte Kryptotext.
     */
    public String encode(String klartext, String schluessel) {
        // Nur mit Großbuchstaben arbeiten
        klartext = klartext.toUpperCase();
        klartext = klartext.replaceAll("[^A-Z]", "");

        // Schlüssel muss eine Zahl >= 2 sein
        int schluesselnr = convertStringToInt(schluessel);
        if (schluesselnr < 2) {
            return "";
        } // end of if

        // Zwischenspeicher zum Aufteilen des Textes auf mehrere Zeilen
        String[] kryptotexte = new String[schluesselnr];

        for (int i = 0; i < schluesselnr; i++) {
            kryptotexte[i] = "";
        } // end of for

        int d = -1;  // Richtung des Zeilendurchlaufs
        int z = 0;   // aktuelle Zeile

        for (int j = 0; j < klartext.length(); j++) {
            char klartext_buchstabe = klartext.charAt(j);
            // Buchstabe in richtiger Zeile eintragen
            kryptotexte[z] += klartext_buchstabe;

            // Wenn in 1. oder letzter Zeile angekommen, dann muss man die Richtung umkehren
            if (z == schluesselnr - 1 || z == 0) {
                d *= -1;
            } // end of if

            // neue Zeile bestimmen
            z += d;
        } // end of for

        // Zeilen zu einem einzigen Kryptotext zusammenführen    
        String kryptotext = "";
        for (int i = 0; i < schluesselnr; i++) {
            kryptotext = kryptotext + kryptotexte[i];
        } // end of for

        return kryptotext;
    }

    /**
     * Entschlüsselt den übergebenen Kryptotext mit dem ZickZack-Verschlüsselungsalgorithmus.
     *
     * @param kryptotext Der zu entschlüsselnde Kryptotext.
     * @param schluessel Der Schlüssel, der für die Entschlüsselung verwendet wird.
     * @return Der entschlüsselte Klartext.
     */
    public String decode(String kryptotext, String schluessel) {
        // Nur mit Großbuchstaben arbeiten
        kryptotext = kryptotext.toUpperCase();
        kryptotext = kryptotext.replaceAll("[^A-Z]", "");
        
        // Schlüssel muss eine Zahl >= 2 sein
        int schluesselnr = convertStringToInt(schluessel);
        if (schluesselnr < 2) {
            return "";
        } // end of if

        // Zwischenspeicher zum Aufteilen des Textes auf mehrere Zeilen
        String[] kryptotexte = new String[schluesselnr];

        for (int i = 0; i < schluesselnr; i++) {
            kryptotexte[i] = "";
        } // end of for

        // Länge der einzelnen Zeilen bestimmen. Erste und letzte Zeile zählen einfach, mittlere doppelt, da man zweifach daran vorbeikommt
        // Länge b haben alle (bzw. 2*b), bei den ersten r Zeilen kommt noch 1 dazu.
        int b = kryptotext.length() / (2 * schluesselnr - 2);
        int r = kryptotext.length() % (2 * schluesselnr - 2);

        // 1. Zeile
        int z;      // Position des Zeilenendes im Kryptotext
        int s = 0;  // Position des Zeilenanfangs im Kryptotext
        z = b;
        if (r > 0) {
            z++;
        } // end of if
        kryptotexte[0] = kryptotext.substring(s, z);
        s += z;

        // Mittlere Zeilen
        for (int i = 1; i < schluesselnr - 1; i++) {
            z += 2 * b;
            if (r > i) {
                z++;
            } // end of if
            if (r > ((schluesselnr - 1) + (schluesselnr - 1 - i))) {
                z++;
            } // end of if
            kryptotexte[i] = kryptotext.substring(s, z);
            s = z;
        } // end of for

        // letzte Zeile
        kryptotexte[schluesselnr - 1] = kryptotext.substring(s);

        // Zeilen wieder zum Klartext zusammenführen
        String klartext = "";

        int d = -1;
        z = 0;

        for (int j = 0; j < kryptotext.length(); j++) {
            klartext += kryptotexte[z].substring(0, 1);
            kryptotexte[z] = kryptotexte[z].substring(1);
            // Bei 1. oder letzter Zeile Richtung umkehren
            if (z == schluesselnr - 1 || z == 0) {
                d *= -1;
            } // end of if
            z += d;
        } // end of for

        return klartext;
    }

    
    // Ende Methoden
}

