package netzwerk;

import java.net.ServerSocket;
import java.net.Socket;

/**
  * Listenerdefinitionen für MyServer und MySocket.
  * Klassen, die mit diesen beiden Klassen arbeiten müssen diese Listener implementieren.
  *
  * @version 1.0 vom 15.11.2012
  * @author Thomas Schaller
  */
  
public interface MySocketListener {            // Der Listener wird �ber folgendes informiert...
   void nachrichtEmpfangen(MySocket s); // am Socket ist eine Nachricht eingegangen. Diese kann dann mit s.holeNachricht() abgeholt werden.
   void verbindungBeendet(MySocket s);  // die Verbindung wurde beendet oder ist zusammengebrochen.
}

