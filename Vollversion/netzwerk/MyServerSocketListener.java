package netzwerk;

import java.net.ServerSocket;
import java.net.Socket;

/**
  * Listenerdefinitionen für MyServer und MySocket.
  * Klassen, die mit diesen beiden Klassen arbeiten müssen diese Listener implementieren.
  *
  * @version 1.0 vom 15.11.2012
  * @author Thomas Schaller
  */
  


public interface MyServerSocketListener extends MySocketListener {
   void neuerClient(MySocket s);        // ein neuer Client hat sich mit dem Server verbunden.
}
