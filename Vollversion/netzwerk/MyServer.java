package netzwerk;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Die Klasse MyServer verwaltet einen ServerSocket. Sie erstellt einen ServerSocket an einem
 * bestimmten Port und wartet dort auf Clients. Es können mehrere Clients gleichzeitig angemeldet sein.
 * Eine Klasse, die diesen Server benutzt, muss MyServerSocketListener implementieren. Sie wird dann
 * über die Methode neuerClient informiert, wenn sich ein neuer Client am Server angemeldet hat.
 * Beschreibung
 *
 *
 * @version 1.0 vom 15.11.2012
 * @author Thomas Schaller
 */

public class MyServer extends Thread {

    // Anfang Attribute
    private ServerSocket ss;
    private int port;
    private boolean aktiv = true;
    private MyServerSocketListener listener;
    // Ende Attribute

    /** Dieser Konstruktor erzeugt einen neuen ServerSocket, startet ihn aber nicht sofort.
     * @param listener Klasse, die MyServerSocketListener implementiert und dann �ber neue Clients und eingehende Nachrichten informiert wird.
     * @param port Port, den der ServerSocket abhört.
     */

    public MyServer(MyServerSocketListener listener, int port) {
        this.port = port;
        this.listener = listener;
        try {
            ss = new ServerSocket(port);
            System.out.println(ss.getLocalSocketAddress());
        } catch(Exception e) {
            System.out.println(e);
        } // end of try
    }

    // Anfang Methoden
    /** Liefert den Port, den der Server abhört.
     * @return Port
     */
    public int getPort() {
        return port;
    }

    /** Startet das Abhören des Ports und das Annehmen der Clients
     */
    public void starten() {
        setDaemon(true);
        start();
    }

    /** Verwaltet das Annehmen der Clients (Diese Methode bitte nicht direkt aufrufen, sondern mit starten() aktivieren).
     */
    public void run() {
        aktiv = true;
        try {
            while(aktiv) {
                System.out.println("Warte auf Client");
                Socket s = ss.accept();
                System.out.println("Client empfangen");
                MySocket ms = new MySocket(s, listener);
                listener.neuerClient(ms);
            }
        } catch(Exception e) {
            System.out.println(e);
        } // end of try

    }
    /** Bricht das Abhören des Ports ab. Es kann nicht wieder durch starten aktiviert werden. Dazu muss ein neuer Server erstellt werden.
     */
    public void stoppen() {
        aktiv = false;
        try{
            ss.close();
            interrupt();
        } catch( Exception e) {
            System.out.println(e);
        }
    }

    /** Meldet, ob der Server aktiv ist.
     * @return true, falls der Server den Port abhört, sonst false.
     */
    public boolean getAktiv() {
        return aktiv;
    }

    // Ende Methoden
} // end of Server

