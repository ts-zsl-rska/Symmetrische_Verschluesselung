package netzwerk;
import javafx.beans.property.SimpleStringProperty;

/**
 * Beschreiben Sie hier die Klasse Nachricht.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Nachricht
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    SimpleStringProperty absender= new SimpleStringProperty();
    SimpleStringProperty kryptotext = new SimpleStringProperty();
    SimpleStringProperty klartext = new SimpleStringProperty();

    /**
     * Konstruktor für Objekte der Klasse Nachricht
     */
    public Nachricht()
    {
    }

    public String getKryptotext() {
        return kryptotext.get();
    }

    public String getAbsender() {
        return absender.get();
    }

    public String getKlartext() {
        return klartext.get();
    }

    public void setKryptotext(String kryptotext){
        this.kryptotext.set(kryptotext);
    }

    public void setKlartext(String klartext){
        this.klartext.set(klartext);
    }

    public void setAbsender(String absender){
        this.absender.set(absender);
    }

}
