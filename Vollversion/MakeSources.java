import de.lautebach.tippfilter.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Beschreiben Sie hier die Klasse MakeSources.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class MakeSources
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen

    /**
     * Konstruktor für Objekte der Klasse MakeSources
     */
    public MakeSources()
    {
    }

    public static void makeRohversion() {

        List<String> options = new ArrayList<String>();
        //add some stuff
        options.add("kopie=..\\SchuelerversionBlueJ");
        options.add("zielgruppe=roh");
        options.add("filter=file://Verschluesselung.java"); 
        options.add("filter=file://CaesarVerschluesselung.java"); 
        options.add("filter=file://CaesarVerschluesselungTest.java"); 
        options.add("filter=file://VigenereVerschluesselung.java"); 
        options.add("filter=file://VigenereVerschluesselungTest.java"); 
        options.add("nimm=file://package.bluej,dir://*");
        options.add("overwrite=yes");
        String[] stringArray = options.toArray(new String[0]);
        Tippfilter.main(stringArray);

    }
}
