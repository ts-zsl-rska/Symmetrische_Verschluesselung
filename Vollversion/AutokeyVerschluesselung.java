/**
 *
 * Beschreibung
 * Autokey Verschlüsselung zur Verwendung im Chatclient
 *
 * @version 1.0 vom 22.6.2017
 * @author Thomas Schaller
 */

public class AutokeyVerschluesselung extends Verschluesselung{
    /**
     * Standardkonstruktor.
     * Ruft den Konstruktor der Elternklasse (Verschluesselung) auf.
     */
    public AutokeyVerschluesselung() {
        super();
    }

    /**
     * Gibt den Namen der Verschlüsselungsmethode zurück.
     *
     * @return Der Name der Autokey-Verschlüsselung.
     */
    public String getName() {
        return "Autokey-Verschlüsselung";
    }

    /**
     * Verschlüsselt den übergebenen Klartext mit dem Autokey-Verschlüsselungsalgorithmus.
     *
     * @param klartext  Der zu verschlüsselnde Klartext.
     * @param schluessel Der Schlüssel, der für die Verschlüsselung verwendet wird.
     * @return Der verschlüsselte Kryptotext.
     */
    public String encode(String klartext, String schluessel) {
        // Nur mit Großbuchstaben arbeiten
        klartext = klartext.toUpperCase();
        klartext = klartext.replaceAll("[^A-Z]", "");

        schluessel = schluessel.toUpperCase();

        String kryptotext = "";

        int k = 0;

        for (int j = 0; j < klartext.length(); j++) {
            char klartext_buchstabe = klartext.charAt(j);

            // Buchstaben j in Zahlen umwandeln
            int nr_klar = getNummer(klartext_buchstabe);

            // Schlüsselbuchstabe ist am Anfang durch Schlüsselwort gegeben, später durch den Anfang vom Text
            int nr_schluessel;
            if (k < schluessel.length()) {
                nr_schluessel = getNummer(schluessel.charAt(k));
            } else {
                nr_schluessel = getNummer(klartext.charAt(k - schluessel.length()));
            }
            k++;

            // Verschlüsseln
            int nr_krypto = (nr_klar + nr_schluessel) % 26;

            // Zahl in Buchstabe umwandeln
            char kryptotext_buchstabe = getChar(nr_krypto);

            // Kryptotext erweitern
            kryptotext = kryptotext + kryptotext_buchstabe;
        }
        return kryptotext;
    }

    /**
     * Entschlüsselt den übergebenen Kryptotext mit dem Autokey-Verschlüsselungsalgorithmus.
     *
     * @param kryptotext Der zu entschlüsselnde Kryptotext.
     * @param schluessel Der Schlüssel, der für die Entschlüsselung verwendet wird.
     * @return Der entschlüsselte Klartext.
     */
    public String decode(String kryptotext, String schluessel) {
        // Nur mit Großbuchstaben arbeiten
        kryptotext = kryptotext.toUpperCase();
        kryptotext = kryptotext.replaceAll("[^A-Z]", "");
        schluessel = schluessel.toUpperCase();

        String klartext = "";

        int k = 0;

        for (int j = 0; j < kryptotext.length(); j++) {
            char kryptotext_buchstabe = kryptotext.charAt(j);

            // Buchstaben j in Zahlen umwandeln
            int nr_krypto = getNummer(kryptotext_buchstabe);

            // Schlüsselbuchstabe ist am Anfang durch Schlüsselwort gegeben, später durch den Anfang vom schon entschlüsselten Text
            int nr_schluessel;
            if (k < schluessel.length()) {
                nr_schluessel = getNummer(schluessel.charAt(k));
            } else {
                nr_schluessel = getNummer(klartext.charAt(k - schluessel.length()));
            }
            k++;

            // Entschlüsseln
            int nr_klartext = (nr_krypto + (26 - nr_schluessel)) % 26;

            // Zahl in Buchstabe umwandeln
            char klartext_buchstabe = getChar(nr_klartext);

            // Klartext erweitern
            klartext = klartext + klartext_buchstabe;
        }
        return klartext;
    }

    // Ende Methoden
}
