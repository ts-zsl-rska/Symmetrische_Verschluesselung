/**
 *
 * Beschreibung
 * Vigenère Verschlüsselung zur Verwendung im Chatclient
 *
 * @version 1.0 vom 22.6.2017
 * @author Thomas Schaller
 */

public class VigenereVerschluesselung extends Verschluesselung{
    public VigenereVerschluesselung() {
        super();
    }

    // Anfang Methoden
    public String getName() {
        return "Vigenère-Verschlüsselung";
    }

    public String encode(String klartext, String schluessel)
    {
        /*#
        String-Befehle:
        s.length()    liefert die Laenge des Strings
        s.charAt(nr)  liefert den Buchstaben (char) an Position nr (man beginnt mit 0 zu zaehlen)
        s = s + "en"; erweitert den String um den String en
        s = s + 'n';  erweitert den String um den String en
        s.toUpperCase() in Grossbuchstaben umwandeln
        s.replaceAll()  Buchstaben ersetzen
        s.toCharArray() in Array von Buchstaben umwandeln
         */

        //# Nur mit Grossbuchstaben arbeiten
        klartext = klartext.toUpperCase();
        klartext = klartext.replaceAll("[^A-Z]","");   // Alle anderen Zeichen löschen: [] definiert einen Bereich, ^ bedeutet "alle außer" => es werden also alle Zeichen außer A-Z durch den leeren String ersetzt, also gelöscht
        schluessel = schluessel.toUpperCase();

        //# Wandle die Strings klartext und schluessel in ein Char-Array um
        //#? lsg-anfang
        char[] aKlartext = klartext.toCharArray();
        char[] aSchluessel = schluessel.toCharArray();
        //#? lsg-ende
        //# Erzeuge ein Char-Array fuer den Kryptotext mit der gleichen Laenge wie der Klartext
        //#? lsg-anfang
        char[] aKryptotext = new char[aKlartext.length];
        //#? lsg-ende

        //# Wiederhole fuer jeden Buchstaben des Klartextes
        //#? lsg-anfang
        for(int i = 0; i < aKlartext.length; i++) {
            //#? lsg-ende
            //# Wandle i. Buchstabe in Zahl um und speichere sie in iKlar
            //#? tipp //# Ziehe dafuer vom Buchstaben einfach 'A' ab (mit char kann man rechnen)
            //#? lsg-anfang
            int iKlar = aKlartext[i] - 'A';
            //#? lsg-ende

            //# Den richtigen Buchstaben (verwende Modulo) des Schluessels in Zahl umrechnen:
            //#? tipp //# int iSchluessel = aSchluessel[i % was muss hier stehen?] - 'A';
            //#? lsg-anfang
            int iSchluessel = aSchluessel[i % aSchluessel.length] - 'A';
            //#? lsg-ende

            //# Verschluessele die Klarzahl mit der Schluesselzahl und speichere in iKrypto
            //#? lsg-anfang
            int iKrypto = (iKlar + iSchluessel) % 26;
            //#? lsg-ende

            //#  Wandle iKrypto in einen Buchstabe um
            //#  Addiere dazu 'A' zu der Zahl und wandle sie explizit mit 
            //#  (char) (iKrypto+'A') in einen Buchstaben um.
            //#  Speichere den Buchstaben an der richtigen Stelle im Array aKryptotext
            //#? lsg-anfang
            aKryptotext[i] = (char) (iKrypto + 'A');

        }      
        // Ende-Wiederholung  
        //#? lsg-ende
        //# Erzeuge einen neuen String aus dem Array aKryptotext
        //# verwende dazu new String(aKryptotext) und gib diesen zurueck
        //#? lsg-anfang
        return new String(aKryptotext);
        //#? lsg-ende
        //#? statt-lsg return "";
    }

    public String decode(String kryptotext, String schluessel)
    {
        schluessel = schluessel.toUpperCase();

        //#? lsg-anfang
        char[] aKryptotext = kryptotext.toCharArray();
        char[] aSchluessel = schluessel.toCharArray();
        char[] aKlartext = new char[aKryptotext.length];

        // Wiederhole fuer jeden Buchstaben des Klartextes
        for(int i = 0; i < aKryptotext.length; i++) {

            // i. Buchstabe in Zahl umwandeln => Klarzahl
            int iKrypto = aKryptotext[i] - 'A';

            // Richtigen Buchstaben des Schluessels in Zahl umrechnen
            int iSchluessel = aSchluessel[i % aSchluessel.length] - 'A';
            
            
            // Kryptozahl entschluesseln
            int iKlar = (iKrypto + (26-iSchluessel)) % 26;

            // Krypto-Zahl in Buchstabe umwandeln
            aKlartext[i] = (char) (iKlar + 'A');

        }      
        // Ende-Wiederholung  
        return new String(aKlartext);
        //#? lsg-ende
        //#? statt-lsg return "";

    }
}
