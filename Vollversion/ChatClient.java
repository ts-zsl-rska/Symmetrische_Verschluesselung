import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class ChatClient extends Application {
    ChatClientController controller; 
    
    @Override
    public void start(Stage primaryStage) {
       try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("view/chatclient.fxml"));
        VBox root = (VBox) loader.load();
        controller = loader.getController();
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
        
    
    } 
       catch(Exception e)    {
        System.out.println(e);
        e.printStackTrace();
    }
    }
    
      @Override
    public void stop() throws Exception{
        controller.beendeVerbindung();
        super.stop();
        // Save file
    }
    
    public static void main(String[] args) {
      launch(args);
    }
}
