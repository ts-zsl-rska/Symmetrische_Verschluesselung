
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import java.util.ArrayList;
import java.io.File;
import java.util.regex.Pattern;
import java.lang.reflect.Modifier;
import netzwerk.*;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.application.Platform;

public class ChatClientController implements MySocketListener {
    @FXML
    private TextField tfName;

    @FXML
    private TextField tfServer;

    @FXML
    private TextField tfPort;

    @FXML
    private Button bVerbinden;

    @FXML
    public Label lStatus;

    @FXML
    private TextArea taKlartext;

    @FXML
    private TextField tfSchluesselEncrypt;

    @FXML
    private ComboBox<String> cbMethodeEncrypt;

    @FXML
    private TextArea taKryptotext;

    @FXML
    private TextField tfSchluesselDecrypt;

    @FXML
    private ComboBox<String> cbMethodeDecrypt;

    @FXML
    private TableView<Nachricht> tNachrichtenKlartext;

    @FXML
    public TableView<Nachricht> tNachrichtenKryptotext;

    @FXML
    private TableColumn<Nachricht, String> tcKryptoAbsender;

    @FXML
    private TableColumn<Nachricht, String> tcKryptoNachricht;

    @FXML
    private TableColumn<Nachricht, String> tcKlarAbsender;

    @FXML
    private TableColumn<Nachricht, String> tcKlarNachricht;

    private ArrayList<Verschluesselung> kryptosysteme;
    private MySocket client = null;

    /**
     * Initialisiert den Controller und setzt die TableColumn-Factories für die Tabellen.
     */
    public void initialize() {
        // Setzt die PropertyValueFactories für die TableColumn-Elemente
        tcKlarAbsender.setCellValueFactory(new PropertyValueFactory<>("absender"));
        tcKryptoAbsender.setCellValueFactory(new PropertyValueFactory<>("absender"));
        tcKlarNachricht.setCellValueFactory(new PropertyValueFactory<>("klartext"));
        tcKryptoNachricht.setCellValueFactory(new PropertyValueFactory<>("kryptotext"));

        kryptosysteme = new ArrayList<Verschluesselung>();

        try {
            // Lädt alle Klassen im aktuellen Verzeichnis und sucht nach Verschluesselung-Implementierungen
            Class v = Class.forName("Verschluesselung");
            String[] entries = new File(".").list();
            for (String name : entries) {
                if (name.endsWith(".class")) {
                    // Erstellt dynamisch eine Klasse aus dem Dateinamen
                    Class c = Class.forName(name.split(Pattern.quote("."))[0]);
                    // Überprüft, ob die Klasse von Verschluesselung erbt und nicht abstrakt ist
                    if (v.isAssignableFrom(c) && !Modifier.isAbstract(c.getModifiers())) {
                        // Erzeugt eine Instanz der gefundenen Klasse
                        Verschluesselung a = ((Verschluesselung)(c).getDeclaredConstructor().newInstance());
                        // Fügt das Verschluesselung-Objekt zur Liste hinzu
                        kryptosysteme.add(a);
                    }
                }
            }
        } catch(Exception e) {
            System.out.println("Exception " + e);
        } 

        // Fügt die Namen der Verschlüsselungsmethoden zur ComboBox hinzu
        for (Verschluesselung v : kryptosysteme) {
            cbMethodeDecrypt.getItems().add(v.getName());
            cbMethodeEncrypt.getItems().add(v.getName());
        }

        // Wählt das erste Element in den ComboBoxen aus
        cbMethodeDecrypt.getSelectionModel().select(0);
        cbMethodeEncrypt.getSelectionModel().select(0);
    }

    /**
     * Event-Handler für den "Entschlüsseln" Button von empfangenen Nachrichten.
     * Entschlüsselt die ausgewählte Nachricht und zeigt den Klartext in der Klartext-Tabelle an.
     *
     * @param event Das ActionEvent, das den Aufruf der Methode ausgelöst hat.
     */
    @FXML
    void bDecryptClicked(ActionEvent event) {
        // Holt die ausgewählte Nachricht aus der Kryptotext-Tabelle
        Nachricht n = tNachrichtenKryptotext.getSelectionModel().getSelectedItem();
        if (n != null) {
            // Holt das ausgewählte Verschlüsselungssystem
            Verschluesselung kryptosystem = kryptosysteme.get(cbMethodeDecrypt.getSelectionModel().getSelectedIndex());
            // Entschlüsselt die Nachricht und setzt den Klartext
            n.setKlartext(kryptosystem.decode(n.getKryptotext(), tfSchluesselDecrypt.getText()));
            // Fügt die entschlüsselte Nachricht zur Klartext-Tabelle hinzu
            tNachrichtenKlartext.getItems().add(0, n);
        } // end of if
    }

    /**
     * Event-Handler für den "Verschlüsseln" Button für abzusendende Nachrichten.
     * Verschlüsselt den Klartext und zeigt den Kryptotext im entsprechenden Textfeld an.
     *
     * @param event Das ActionEvent, das den Aufruf der Methode ausgelöst hat.
     */
    @FXML
    void bEncryptClicked(ActionEvent event) {
        // Holt das ausgewählte Verschlüsselungssystem
        Verschluesselung kryptosystem = kryptosysteme.get(cbMethodeEncrypt.getSelectionModel().getSelectedIndex());
        // Verschlüsselt den Klartext und setzt den Kryptotext
        taKryptotext.setText(kryptosystem.encode(taKlartext.getText(), tfSchluesselEncrypt.getText()));
        // Leert das Klartext-Feld
        taKlartext.setText("");
    }

    /**
     * Event-Handler für den "Entschlüsseln" Button für abzusendende Nachrichten.
     * Entschlüsselt den Kryptotext im entsprechenden Textfeld und zeigt den Klartext an.
     *
     * @param event Das ActionEvent, das den Aufruf der Methode ausgelöst hat.
     */
    @FXML
    void bDecryptMessageClicked(ActionEvent event) {
        // Holt das ausgewählte Verschlüsselungssystem
        Verschluesselung kryptosystem = kryptosysteme.get(cbMethodeEncrypt.getSelectionModel().getSelectedIndex());
        // Entschlüsselt den Kryptotext und setzt den Klartext
        taKlartext.setText(kryptosystem.decode(taKryptotext.getText(), tfSchluesselEncrypt.getText()));
        // Leert das Kryptotext-Feld
        taKryptotext.setText("");
    }

    /**
     * Event-Handler für den "Senden" Button.
     * Sendet den aktuellen Kryptotext an den Server, sofern der Client aktiv ist.
     * Falls nicht, aktiviert er die Eingabefelder für die Verbindungseinstellungen.
     *
     * @param event Das ActionEvent, das den Aufruf der Methode ausgelöst hat.
     */
    @FXML
    void bSendenClicked(ActionEvent event) {
        if (client != null && client.isAktiv() && !taKryptotext.getText().equals("")) {
            // Sendet den Kryptotext an den Server
            client.sendeNachricht(taKryptotext.getText());
            // Leert das Kryptotext-Feld
            taKryptotext.setText("");
        } else {
            // Aktiviert die Eingabefelder für die Verbindungseinstellungen
            tfName.setDisable(false);
            tfServer.setDisable(false);
            tfPort.setDisable(false);
            bVerbinden.setDisable(false);
        } // end of if
    }

    /**
     * Event-Handler für den "Verbinden" Button.
     * Verwaltet die Verbindung zum Server basierend auf dem aktuellen Status des Clients.
     * Wenn der Client bereits aktiv ist, wird die Verbindung getrennt. Andernfalls wird eine neue Verbindung hergestellt.
     *
     * @param event Das ActionEvent, das den Aufruf der Methode ausgelöst hat.
     */
    @FXML
    void bVerbindenClicked(ActionEvent event) {
        // Holt Serveradresse und Port aus den Textfeldern
        String a = tfServer.getText();
        int p = Integer.parseInt(tfPort.getText());

        if (client != null && client.isAktiv()) {
            // Wenn der Client aktiv ist, wird die Verbindung getrennt
            tfName.setDisable(false);
            tfServer.setDisable(false);
            tfPort.setDisable(false);
            bVerbinden.setText("Verbinden");
            // Sendet eine Nachricht an den Server, um die Verbindung zu beenden
            client.sendeNachricht("exit");
            // Trennt die Verbindung
            client.trenneVerbindung();
        } else {
            // Erstellt einen neuen Client mit den angegebenen Verbindungsinformationen
            client = new MySocket(a, p, this);
            if (client.isAktiv()) {
                // Wenn die Verbindung erfolgreich hergestellt wurde, deaktiviert die Eingabefelder und ändert den Button-Text
                tfName.setDisable(true);
                tfServer.setDisable(true);
                tfPort.setDisable(true);
                bVerbinden.setText("Trennen");
                // Sendet eine Nachricht mit dem Benutzernamen an den Server
                client.sendeNachricht("user:" + tfName.getText());
            }
        } // end of if-else
    }

    /**
     * Methode, die aufgerufen wird, wenn eine Nachricht vom Server empfangen wird.
     * Zeigt die empfangene Nachricht in der Benutzeroberfläche an.
     *
     * @param client Der MySocket-Client, von dem die Nachricht empfangen wurde.
     */
    public void nachrichtEmpfangen(MySocket client) {
        // Holt die empfangene Nachricht
        String s = client.holeNachricht();
        if (s.contains(">")) {
            // Wenn die Nachricht von einem Chatteilnehmer ist, wird sie in die Kryptotext-Tabelle eingefügt
            String[] t = s.split(">");
            Nachricht n = new Nachricht();
            n.setAbsender(t[0]);
            n.setKryptotext(t[1]);
            // Fügt die Nachricht in die Tabelle ein und wählt sie aus
            Platform.runLater(new Runnable() {
                    public void run() {
                        tNachrichtenKryptotext.getItems().add(0, n);
                        tNachrichtenKryptotext.getSelectionModel().select(n);
                    }
                });
        } else {
            // Wenn die Nachricht eine allgemeine Meldung ist, wird sie in der Status-Label angezeigt
            Platform.runLater(new Runnable() {
                    public void run() {
                        lStatus.setText(s);
                    }
                });
        }
    }

    /**
     * Methode, die aufgerufen wird, wenn die Verbindung zum Server beendet wurde.
     * Aktualisiert die Benutzeroberfläche entsprechend.
     *
     * @param client Der MySocket-Client, der die Verbindung beendet hat.
     */
    public void verbindungBeendet(MySocket client) {
        // Setzt den Client auf null, um eine neue Verbindung zuzulassen
        client = null;
        // Aktualisiert die Benutzeroberfläche
        Platform.runLater(new Runnable() {
                public void run() {
                    tfName.setDisable(true);
                    tfServer.setDisable(true);
                    tfPort.setDisable(true);
                    bVerbinden.setText("Verbinden");
                    lStatus.setText("Verbindung wurde getrennt.");
                }
            });
    }

    /**
     * Methode, um die Verbindung zum Server zu beenden.
     * Sendet eine "exit"-Nachricht an den Server und trennt die Verbindung.
     */
    public void beendeVerbindung() {
        if (client != null) {
            // Sendet eine "exit"-Nachricht an den Server und trennt die Verbindung
            client.sendeNachricht("exit");
            client.trenneVerbindung();
        } // end
    }

}
