/**
 *
 * Beschreibung
 * Caesar Verschuesselung zur Verwendung im Chatclient
 *
 * @version 1.0 vom 22.6.2017
 * @author Thomas Schaller
 */

public class CaesarMultVerschluesselung extends Verschluesselung{

    public CaesarMultVerschluesselung() {
        super();
    }

    // Anfang Methoden
    public String getName() {
        return "multipl. Caesar-Verschl.";
    }

    /**
     * Verschlüsselt den übergebenen Klartext mit dem multiplikativen Cäsar-Verschlüsselungsalgorithmus.
     *
     * @param klartext  Der zu verschlüsselnde Klartext.
     * @param schluessel Der Schlüssel, der für die Verschlüsselung verwendet wird.
     * @return Der verschlüsselte Kryptotext.
     */
    public String encode(String klartext, String schluessel)
    {
        /*#
        String-Befehle:
        s.length()    liefert die Laenge des Strings
        s.charAt(nr)  liefert den Buchstaben (char) an Position nr (man beginnt mit 0 zu zaehlen)
        s = s + "en"; erweitert den String um den String en
        s = s + 'n';  erweitert den String um den String en
        s.toUpperCase() in Grossbuchstaben umwandeln
        s.replaceAll()  Buchstaben ersetzen
        s.toCharArray() in Array von Buchstaben umwandeln
         */

        //# Nur mit Grossbuchstaben arbeiten
        klartext = klartext.toUpperCase();
        klartext = klartext.replaceAll("[^A-Z]","");   // Alle anderen Zeichen l�schen: [] definiert einen Bereich, ^ bedeutet "alle au�er" => es werden also alle Zeichen au�er A-Z durch den leeren String ersetzt, also gel�scht
        schluessel = schluessel.toUpperCase();

        //# Wandle die Strings klartext und schluessel in ein Char-Array um
        //#? lsg-anfang
        char[] aKlartext = klartext.toCharArray();
        char[] aSchluessel = schluessel.toCharArray();
        //#? lsg-ende
        //# Erzeuge ein Char-Array fuer den Kryptotext mit der gleichen Laenge wie der Klartext
        //#? lsg-anfang
        char[] aKryptotext = new char[aKlartext.length];
        //#? lsg-ende

        //# Ersten Buchstaben des Schluessels in Zahl umrechnen:
        //#? tipp //# Ziehe dafuer vom Buchstaben einfach 'A' ab (mit char kann man rechnen)
        //#? tipp //# int iSchluessel = aSchluessel[0] - 'A';
        //#? lsg-anfang
        int iSchluessel = aSchluessel[0] - 'A';
        //#? lsg-ende

        //# Wiederhole fuer jeden Buchstaben des Klartextes
        //#? lsg-anfang
        for(int i = 0; i < aKlartext.length; i++) {
            //#? lsg-ende
            //# Wandle i. Buchstabe in Zahl um und speichere sie in iKlar
            //#? lsg-anfang
            int iKlar = aKlartext[i] - 'A';
            //#? lsg-ende

            //# Verschluessele die Klarzahl mit der Schluesselzahl und speichere in iKrypto
            //#? lsg-anfang
            int iKrypto = (iKlar * iSchluessel) % 26;
            //#? lsg-ende

            //#  Wandle iKrypto in einen Buchstabe um
            //#  Addiere dazu 'A' zu der Zahl und wandle sie explizit mit 
            //#  (char) (iKrypto+'A') in einen Buchstaben um.
            //#  Speichere den Buchstaben an der richtigen Stelle im Array aKryptotext
            //#? lsg-anfang
            aKryptotext[i] = (char) (iKrypto + 'A');

        }      
        // Ende-Wiederholung  
        //#? lsg-ende
        //# Erzeuge einen neuen String aus dem Array aKryptotext
        //# verwende dazu new String(aKryptotext) und gib diesen zurueck
        //#? lsg-anfang
        return new String(aKryptotext);
        //#? lsg-ende
        //#? statt-lsg return "";
    }

    /**
     * Berechnet das multiplikative Inverse zu i modulo 26 unter Verwendung der Brute-Force-Methode.
     *
     * @param i Der Wert, für den das multiplikative Inverse gefunden werden soll.
     * @return Das multiplikative Inverse zu i modulo 26 oder -1, falls keines gefunden wurde.
     */
    public static int berechneMultiplikativesInverse(int i) {
        int m = 26; // Modulo-Wert (in diesem Fall 26 für das lateinische Alphabet)

        for (int x = 1; x < m; x++) {
            if ((i * x) % m == 1) {
                return x;
            }
        }

        // Falls kein multiplikatives Inverses gefunden wurde, gib 1 zurück
        return 1;
    }
    
    /**
     * Entschlüsselt den übergebenen Kryptotext mit dem multiplikativen Cäsar-Verschlüsselungsalgorithmus.
     *
     * @param kryptotext Der zu entschlüsselnde Kryptotext.
     * @param schluessel Der Schlüssel, der für die Entschlüsselung verwendet wird.
     * @return Der entschlüsselte Klartext.
     */
    public String decode(String kryptotext, String schluessel)
    {
        schluessel = schluessel.toUpperCase();

        //#? lsg-anfang
        char[] aKryptotext = kryptotext.toCharArray();
        char[] aSchluessel = schluessel.toCharArray();
        char[] aKlartext = new char[aKryptotext.length];

        // Ersten Buchstaben des Schluessels in Zahl umrechnen
        int iSchluessel = aSchluessel[0] - 'A';

        // Wiederhole fuer jeden Buchstaben des Klartextes
        for(int i = 0; i < aKryptotext.length; i++) {

            // i. Buchstabe in Zahl umwandeln => Klarzahl
            int iKrypto = aKryptotext[i] - 'A';

            // Kryptozahl entschluesseln
            int multInvers = berechneMultiplikativesInverse(iSchluessel);
            int iKlar = (iKrypto * multInvers) % 26;

            // Krypto-Zahl in Buchstabe umwandeln
            aKlartext[i] = (char) (iKlar + 'A');

        }      
        // Ende-Wiederholung  
        return new String(aKlartext);
        //#? lsg-ende
        //#? statt-lsg return "";
    }

    // Ende Methoden
}
