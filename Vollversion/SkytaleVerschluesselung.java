/**
  *
  * Beschreibung
  * Skytale Verschlüsselung zur Verwendung im Chatclient
  *
  * @version 1.0 vom 22.6.2017
  * @author Thomas Schaller
  */


public class SkytaleVerschluesselung extends Verschluesselung {

    /**
     * Standardkonstruktor.
     * Ruft den Konstruktor der Elternklasse (Verschluesselung) auf.
     */
    public SkytaleVerschluesselung() {
        super();
    }

    /**
     * Gibt den Namen der Verschlüsselungsmethode zurück.
     *
     * @return Der Name der Skytale-Verschlüsselung.
     */
    public String getName() {
        return "Skytale-Verschlüsselung";
    }

    /**
     * Verschlüsselt den übergebenen Klartext mit dem Skytale-Verschlüsselungsalgorithmus.
     *
     * @param klartext  Der zu verschlüsselnde Klartext.
     * @param schluessel Der Schlüssel, der für die Verschlüsselung verwendet wird.
     * @return Der verschlüsselte Kryptotext.
     */
    public String encode(String klartext, String schluessel) {
        // Nur mit Großbuchstaben arbeiten
        klartext = klartext.toUpperCase();
        klartext = klartext.replaceAll("[^A-Z]", "");

        // Schlüssel ist eine Zahl
        int schluesselnr = convertStringToInt(schluessel);

        String kryptotext = "";

        // Z.B. bei Schlüssel 4: Im 1. Durchgang 1./5./9. usw Buchstabe nehmen, im 2. Durchgang 2./6./10. usw.
        for (int i = 0; i < schluesselnr; i++) {  // Durchgänge
            for (int k = i; k < klartext.length(); k += schluesselnr)  // Schleife mit Sprungweite schluesselnr
            {
                char klartext_buchstabe = klartext.charAt(k);

                // Kryptotext erweitern
                kryptotext = kryptotext + klartext_buchstabe;
            }
        } // end of for
        return kryptotext;
    }

    /**
     * Entschlüsselt den übergebenen Kryptotext mit dem Skytale-Verschlüsselungsalgorithmus.
     *
     * @param kryptotext Der zu entschlüsselnde Kryptotext.
     * @param schluessel Der Schlüssel, der für die Entschlüsselung verwendet wird.
     * @return Der entschlüsselte Klartext.
     */
    public String decode(String kryptotext, String schluessel) {
        // Nur mit Großbuchstaben arbeiten
        kryptotext = kryptotext.toUpperCase();
        kryptotext = kryptotext.replaceAll("[^A-Z]", "");
        int schluesselnr = convertStringToInt(schluessel);

        String klartext = "";

        int k = 0;
        int j = 0;
        int d = 0;

        // Länge der Abschnitte bestimmen
        int b = kryptotext.length() / schluesselnr;
        int r = kryptotext.length() % schluesselnr;

        while (kryptotext.length() > klartext.length()) {
            klartext += kryptotext.charAt(j);

            // Zur neuen Position springen
            j = j + b;

            // Die ersten r Mal kommt noch 1 dazu
            if (k < r) {
                j++;
            } // end of if
            k++;

            // Ende Kryptotext erreicht. Wieder vorne beim nächsten Buchstaben beginnen
            if (j >= kryptotext.length()) {
                d++;
                j = d;
                k = 0;
            } // end of if
        } // end of while

        return klartext;
    }
    
    // Weitere Methoden könnten hier hinzugefügt werden

    // Ende Methoden

}

