import java.util.ArrayList;
import netzwerk.*;

/**
 * Server für ein Chatprogramm.
 *
 * @version 1.0 vom 22.06.2017
 * @author Thomas Schaller
 */
public class ChatServer implements MyServerSocketListener {

    // Anfang Attribute

    // Liste der verbundenen Clients
    private ArrayList<MySocket> clients;

    // Liste der Benutzernamen der verbundenen Clients
    private ArrayList<String> names;

    // Flag zur Verwaltung des Serverlebenszyklus
    private static boolean ende;

    // Instanz des MyServer-Objekts für die Serverkommunikation
    private MyServer s;

    // Ende Attribute

    // Anfang Methoden
    /**
     * Standardkonstruktor für den ChatServer. Setzt den Port auf 44444.
     */
    public ChatServer() {
        this(44444);
    }

    /**
     * Konstruktor für den ChatServer mit benutzerdefiniertem Port.
     *
     * @param port Der Port, auf dem der Server lauschen soll.
     */
    public ChatServer(int port) {
        clients = new ArrayList<MySocket>();
        names = new ArrayList<String>();
        s = new MyServer(this, port);
        s.starten();
    }

    /**
     * Methode, um einen neuen Client zu behandeln, der sich mit dem Server verbindet.
     *
     * @param client Der neu verbundene Client.
     */
    public void neuerClient(MySocket client) {
        this.clients.add(client);
        if (names.size() == 0) {
            client.sendeNachricht("Zur Zeit ist niemand online.");
        }
        if (names.size() == 1) {
            client.sendeNachricht("Zur Zeit ist nur " + names.get(0) + " online.");
        }
        if (names.size() > 1) {
            client.sendeNachricht("Zur Zeit sind " + names.toString() + " online.");
        }
        System.out.println("Habe Kontakt mit " + client.getSocket().getInetAddress() + " Port " + client.getSocket().getPort());
        this.names.add("?");
    }

    /**
     * Methode, um eine empfangene Nachricht von einem Client zu behandeln.
     *
     * @param client Der Client, von dem die Nachricht empfangen wurde.
     */
    public void nachrichtEmpfangen(MySocket client) {
        String s = client.holeNachricht();
        System.out.println("Habe Nachricht empfangen:" + s);
        int i = clients.indexOf(client);
        if (s.equals("exit:")) {
            clients.remove(i);
            names.remove(i);
            client.trenneVerbindung();
        } else {
            if (s.length() >= 5 && s.substring(0, 5).equals("user:")) {
                if (i >= names.size()) {
                    // Hier könnte ein neuer Benutzer hinzugefügt werden
                } else {
                    names.set(i, s.substring(5));
                }
                for (MySocket c : clients) {
                    if (c != client) {
                        c.sendeNachricht("Neuer User: " + s.substring(5));
                    }
                }

            } else {
                for (MySocket c : clients) {
                    c.sendeNachricht(names.get(i) + ">" + s);
                }
            }
        }
    }

    /**
     * Methode, um eine beendete Verbindung mit einem Client zu behandeln.
     *
     * @param client Der Client, mit dem die Verbindung beendet wurde.
     */
    public void verbindungBeendet(MySocket client) {
        int i = clients.indexOf(client);
        clients.remove(i);
        System.out.println("Verbindung mit " + client.getSocket().getInetAddress() + " Port: " + client.getSocket().getPort() + " verloren.");
        for (MySocket c : clients) {
            if (c != client) {
                c.sendeNachricht("User abgemeldet: " + names.get(i));
            }
        }
        names.remove(i);
    }
    // Ende Methoden
} // end of ChatServer
